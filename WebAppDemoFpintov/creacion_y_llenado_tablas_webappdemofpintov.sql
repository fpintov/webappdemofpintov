-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-02-2022 a las 14:22:03
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 8.1.2


SET time_zone = "-03:00";

--
-- Base de datos: `demo_fpintov`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turnos`
--

CREATE TABLE `turnos` (
  `id_turno` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_turno` varchar(50) NOT NULL,
  `descr_turno` varchar(100) NOT NULL,
   PRIMARY KEY (id_turno)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `turnos`
--

INSERT INTO `turnos` (`id_turno`, `nombre_turno`, `descr_turno`) VALUES
(1, 'Administrativo', 'Lunes a viernes 08:00 a 18:00 hrs'),
(2, 'Jornada_Mañana', 'Lunes a Sábado de 08:00 a 16:00 hrs.'),
(3, 'Jornada_Tarde', 'Lunes a Sábado de 16:00 a 01:00am hrs.'),
(4, 'Articulo 22', 'Sin restricción de horarios.');

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id_cargo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cargo` varchar(50) NOT NULL,
  `descr_cargo` varchar(200) DEFAULT NULL,
   PRIMARY KEY (id_cargo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id_cargo`, `nombre_cargo`, `descr_cargo`) VALUES
(1, 'Supervisor', NULL),
(2, 'Soldador', NULL),
(3, 'Ayudante Soldador', NULL),
(4, 'Chofer', NULL),
(5, 'Electromecánico', NULL),
(6, 'Ayudante Electromecánico', NULL),
(7, 'Prevencionista de Riesgos', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(10) NOT NULL,
  PRIMARY KEY (id_estado)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id_estado`, `estado`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadores`
--

CREATE TABLE `trabajadores` (
  `rut` varchar(9) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `apellido` varchar(80) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `password` varchar(20) NOT NULL,
  `fech_nac` date DEFAULT NULL,
  `fech_ing` date DEFAULT NULL,
  `cargo_id` int(11),
  `turno_id` int(11),
  `estado_id` int(11),

  PRIMARY KEY (rut),
    INDEX (cargo_id),
    FOREIGN KEY (cargo_id) REFERENCES cargos(id_cargo),
    INDEX (turno_id),
    FOREIGN KEY (turno_id) REFERENCES turnos(id_turno),
    INDEX (estado_id),
    FOREIGN KEY (estado_id) REFERENCES estados(id_estado)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `trabajadores`
--

INSERT INTO `trabajadores` (`rut`, `nombre`, `apellido`, `email`, `telefono`, `password`, `fech_nac`, `fech_ing`, `cargo_id`, `turno_id`, `estado_id`) VALUES
('132438900', 'Francisco', 'Pinto', 'fpintov1@gmail.com', '+56 954 546 853', '13243', NULL, NULL, 1, 1, 1),
('133396241', 'Clara', 'Solovera', 'clarasolovera@hotmail.com', '+56 999 111 222', '13339', NULL, NULL, 1, 1, 1),
('164765180', 'Nayadeth', 'Bermúdez', 'nayadethb1@gmail.com', '+56 975 112 276', '16476', NULL, NULL, 1, 1, 1),
('273322159', 'Júpiter', 'Pinto', 'jupiterpinto@gmail.com', '+56 999 888 777', '27332', NULL, NULL, 1, 1, 1),
('66001261', 'Julio', 'Pinto', 'juliopintotata@gmail.com', '+56 999 666 555', '66001', NULL, NULL, 1, 1, 1);

-- --------------------------------------------------------



