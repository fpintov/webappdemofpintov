package com.webappdemo.webappdemofpintov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebAppDemoFpintovApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebAppDemoFpintovApplication.class, args);
    }

}
